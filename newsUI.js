class NewsUI {
	constructor () {
		this.articles = [];
	}

	renderAllNews (arrayNews) {
		this.articles = arrayNews;
		let main = document.querySelector('.main');
			 main.innerHTML = '';
		this.articles.forEach( (news) => {
			let node = this.renderOneNews(news)
			main.appendChild(node)
			//append child to main div
		});
	}
	renderOneNews(news) {
		// console.log(news.title)
		let conteiner = document.createElement('div');
			conteiner.classList.add('width')
			let title = document.createElement('a');
			title.textContent = news.title;
			title.setAttribute('href', news.url,)
			title.setAttribute('target', '_blank')
			let image = document.createElement('img');
			image.setAttribute('src', news.urlToImage)
			image.style.maxWidth = '100%';
			image.style.maxHeight = 'auto';
			let description = document.createElement('p')
			description.textContent = news.description;
			let publishedAt = document.createElement('h5')
				publishedAt.textContent = news.publishedAt;
				publishedAt.style.textAlign = 'right';
			let author = document.createElement('h5');
			author.textContent = news.author;
			author.style.textAlign = 'right';
			conteiner.appendChild(title)
			conteiner.appendChild(image)
			conteiner.appendChild(description)
			conteiner.appendChild(publishedAt)
			conteiner.appendChild(author)

		return conteiner
	}

}
