class NewsService {
    constructor() {
        this.apiUrl = 'https://newsapi.org/v2/top-headlines?';
        this.apiKey = '9c27b0f722b84da5a08312d2b125351b';
        this.country = 'ua';
        this.category = 'sports';
        this.q = '';
        
    }

    getData(country = this.country, category = this.category, q=this.q) {
        let url = this.apiUrl + 'country=' +
            country + '&category=' + category +'&apiKey=' + this.apiKey + '&q=' + q;
        return fetch(url)
            .then((response) => response.json())
            .then((response) => response)
            .catch((error) => console.error(error))
    }
}